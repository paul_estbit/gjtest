# Readme

This README explains how to setup and run GJTest project.

## Installation

To run this project successfully you will need:

 - NODE >=7.7.4
 - Redis
 - NPM

To install the project

 1. unpack
 2. cd to directory in terminal
 3. run `npm i`

## Running the project
### In the project root directory 

To run the project:
`redis-start`
`node app/app.js`

To send test post data:
`node app/client.js`