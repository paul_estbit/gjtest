/*////////////////////////////////////////////
----    GJ Test App
///////////////////////////////////////////*/

// Import dependencies
const express = require('express'); // Import Express
const redis = require("redis");// Import Redis
const kue = require('kue'); // Import Kue

// Create an express instance
const app = express();
// Add body-parser
const bodyParser = require('body-parser');

// Apply body-parser
app.use(bodyParser.json());

// Create Redis client
const client = redis.createClient();

// Create Kue queue
const queue = kue.createQueue();

// Start the server
app.listen(3000, ()=>{

    // Output the welcome message
    console.log('😈 App is accessible on http://127.0.0.1:3000 😈');

});

// Handle post requests to /api/import
app.post('/api/import',(req,res)=>{

    // Output the receiv data
    console.log(`→ Received HTTP Request - ${JSON.stringify(req.body.load)}`);

    // Add data to Kue's processing queue
    pushToQueue(req.body.load);

    // End the response
    res.end();

});

/**
 * Pushes job to queue
 * @constructor
 * @param {object} data - Data to be attached to the job
 */
function pushToQueue(data){

    // Log the action
    console.log(`⚙︎ Push job to queue - ${JSON.stringify(data)}`);

    // Create new Kue job with passed data
    const job = queue
                    .create('hotel',data)
                    .removeOnComplete( true )
                    .save();

}

function testJobs(){

    const data ={'test_key':'test_value'};

    for(let i = 0; i < 5; i++){

        pushToQueue(data);

    }

}

// testJobs()

// Kue queue process
queue.process('hotel',(job, ctx, done)=>{

    // Delay the process execution for 10 seconds
    ctx.pause( 5000, function(err){

        // Log the 'completed' status
        console.log(`✅ Finished processing job in - ${JSON.stringify(job.data)}`);

        // Continue with the execution
        setTimeout( function(){ ctx.resume(); }, 10000 );

    });

    // Log the 'started' status
    console.log(`⚙︎ Started processing job - ${JSON.stringify(job.data)}`);

    // Complete the job
    done();

});
