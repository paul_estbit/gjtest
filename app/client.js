/*////////////////////////////////////////////
----    GJ Test App Client
///////////////////////////////////////////*/

// Import dependencies
const axios = require('axios');

// Post a set of test data, exit when done
axios.post('http://127.0.0.1:3000/api/import',{

    load: {

        "hotelId": "hotel1",
        "reservations": [
            { "reservationId": "reservation1", "name": "guest1" },
            { "reservationId": "reservation2", "name": "guest2" }
        ]

    }

}).then(()=>{ process.exit(0) });
